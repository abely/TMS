-- Database: "TMSdb"
 --Hello
-- DROP DATABASE "TMSdb";

CREATE DATABASE "TMSdb"
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'en_US.UTF-8'
       LC_CTYPE = 'en_US.UTF-8'
       CONNECTION LIMIT = -1;


-- Table: tb_personnel
   
-- DROP TABLE tb_personnel;


CREATE TABLE tb_personnel
(
  id serial NOT NULL,
  employee_id character varying(55) NOT NULL,
  company_id character varying(55) NOT NULL,
  cn_name character varying(55) NOT NULL,
  department character varying(100) NOT NULL,
  section character varying(100) DEFAULT NULL::character varying,
  position_name character varying(100) DEFAULT NULL::character varying,
  superior_name character varying(100) DEFAULT NULL::character varying,
  remark_desc character varying(100) DEFAULT NULL::character varying,
  em_classic character varying(100) DEFAULT NULL::character varying,
  CONSTRAINT tb_personnel_pkey PRIMARY KEY (id),
  CONSTRAINT tb_personnel_company_id_key UNIQUE (company_id),
  CONSTRAINT tb_personnel_employee_id_key UNIQUE (employee_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tb_personnel
  OWNER TO postgres;
-- Table: tb_train_course

-- DROP TABLE tb_train_course;

CREATE TABLE tb_train_course
(
  id serial NOT NULL,
  course_id character varying(55) NOT NULL,
  course_name character varying(100) NOT NULL,
  course_classic character varying(100) NOT NULL,
  course_desc character varying(100) DEFAULT NULL::character varying,
  create_date date DEFAULT ('now'::text)::date,
  CONSTRAINT tb_train_course_pkey PRIMARY KEY (id),
  CONSTRAINT tb_train_course_course_id_key UNIQUE (course_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tb_train_course
  OWNER TO postgres;
-- Table: tb_train_recrod

-- DROP TABLE tb_train_recrod;

CREATE TABLE tb_train_recrod
(
  id serial NOT NULL,
  course_id character varying(55) NOT NULL,
  course_detail character varying(100) NOT NULL,
  train_date date DEFAULT ('now'::text)::date,
  train_type character varying(55) NOT NULL,
  train_teach_id integer NOT NULL,
  stu_count bigint DEFAULT 0,
  CONSTRAINT tb_train_recrod_pkey PRIMARY KEY (id),
  CONSTRAINT tb_train_record_fk_course_id_fkey FOREIGN KEY (course_id)
      REFERENCES tb_train_course (course_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT tb_train_record_fk_train_teach_id_fkey FOREIGN KEY (train_teach_id)
      REFERENCES tb_trainteacher (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tb_train_recrod
  OWNER TO postgres;

-- Table: tb_train_stu

-- DROP TABLE tb_train_stu;

CREATE TABLE tb_train_stu
(
  ref_id serial NOT NULL,
  per_id character varying NOT NULL,
  rec_id integer NOT NULL,
  train_result character varying(55) DEFAULT NULL::character varying,
  CONSTRAINT tb_train_stu_pkey PRIMARY KEY (ref_id),
  CONSTRAINT tb_train_stu_fk_per_id_fkey FOREIGN KEY (per_id)
      REFERENCES tb_personnel (employee_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT tb_train_stu_fk_rec_id_fkey FOREIGN KEY (rec_id)
      REFERENCES tb_train_recrod (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tb_train_stu
  OWNER TO postgres;

-- Table: tb_trainteacher

-- DROP TABLE tb_trainteacher;

CREATE TABLE tb_trainteacher
(
  id serial NOT NULL,
  teacher_type character varying(55) NOT NULL,
  out_teach_name character varying(55) DEFAULT NULL::character varying,
  out_teach_company character varying(100) DEFAULT NULL::character varying,
  out_train_money character varying(55) DEFAULT NULL::character varying,
  out_train_phone character varying(55) DEFAULT NULL::character varying,
  train_desc character varying(100) DEFAULT NULL::character varying,
  em_id character varying(55) DEFAULT NULL::character varying,
  CONSTRAINT tb_trainteacher_pkey PRIMARY KEY (id),
  CONSTRAINT "tb_trainteacher_fk_emID_fkey" FOREIGN KEY (em_id)
      REFERENCES tb_personnel (employee_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tb_trainteacher
  OWNER TO postgres;
