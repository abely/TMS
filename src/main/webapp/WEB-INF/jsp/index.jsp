<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title></title>
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/extjs/build/classic/theme-crisp/resources/theme-crisp-all.css">

<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/static/css/main.css">

<script type="text/javascript" src="<%=request.getContextPath()%>/extjs/ext-bootstrap.js" charset="utf-8"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/extjs/build/classic/local/locale-zh_CN-debug.js" charset="utf-8"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/tms/Application.js" charset="utf-8"></script>

</head>
<body>
</body>
</html>
