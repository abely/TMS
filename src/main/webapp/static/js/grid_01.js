Ext.onReady(function() {
	var itemsPerPage = 1;
	Ext.define('User', {
		extend : 'Ext.data.Model',
		fields : [ {
			name : 'id',
			type : 'int'
		}, {
			name : 'name',
			type : 'string'
		}, {
			name : 'phone',
			type : 'string',
			mapping : 'phoneNumber'
		} ]
	});

	Ext.define('MyController', {
		extend : 'Ext.app.ViewController',
		alias : 'controller.userList',
		init : function() {
		},
		onClick : function() {
			Ext.Msg.alert("Hello", "World");
		}

	})
	var store = Ext.create('Ext.data.Store', {
		model : 'User',
		proxy : {
			type : 'ajax',
			url : 'static/json/user.json',
			reader : {
				type : 'json',
				rootProperty : 'users'
			}
		},
	// autoLoad:true
	});
	store.load({
		params : {
			start : 0,
			limit : itemsPerPage
		}
	});
	Ext.create('Ext.grid.Panel', {
		title : 'MyGrid',
		layout : 'fit',
		closable : true,
		collapsible : true, // 可折叠
		autoScroll : true, // 自动创建滚动条
		width : 400,
		hight : 600,
		controller:'userList',
		draggable : true,
		fixed : false,
		columnLines : true,
		store : Ext.data.StoreManager.lookup(store),
		selModel : 'cellmodel',
		columnWidth : 100,
		tbar : [ {
			xtype : 'button',
			iconCls : 'x-btn-over',
			text : 'Button',
			listeners:{
				click:'onClick'
			}
		} ],
		columns : {
			items : [ {
				text : 'ID',
				dataIndex : 'id',
				editor : 'textfield'
			}, {
				text : 'Name',
				dataIndex : 'name'
			}, {
				text : 'Phone',
				dataIndex : 'phone'
			} ]
		},
		plugins : [ {
			ptype : 'cellediting',
			clicksToEdit : 1
		}, {
			ptype : 'gridviewdragdrop',
			dragText : 'Drag and drop to reorganize'
		} ],
		dockedItems : [ {
			xtype : 'pagingtoolbar',
			store : store, // same store GridPanel is using
			dock : 'top',
			displayInfo : true
		} ],
		renderTo : Ext.getBody()
	});
	
});
