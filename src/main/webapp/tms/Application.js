Ext.application({
	requires : [ 'TMS.*','Ext.container.Viewport','TMS.view.menuview.MenuviewList',
	'TMS.view.course.CourseView'],
	name : 'TMS',
	appFolder : 'tms/app',
	// controllers : ['user.UsersController'],
	views:['menuview.MenuView','course.CourseView','courserecord.CourseRecord'],
	stores:['Personnels','Courses','Students'],
	// controllers:[],
	// models:['Student','Course'],
	launch : function() {
		Ext.create('Ext.container.Viewport', {
			layout : 'border',
			items: [{
				title: 'South Region is resizable',
		        region: 'south',     // position for region
		        xtype: 'container',
		        height: 30,
		        // split: true,         // enable resizing
		        margin: '0 5 5 5'
		    },{
		    	xtype:'container',
		    	region: 'center', 
		    	layout : 'border',
		    	margin: '1 1 1 1',
		    	items:[{
		    		region : 'west',
					collapsible : true,
					title : '管理菜单',
					width : 160,
					layout:'fit',
					// split: true,
					items:[{
						xtype:'menuview',
						// title: 'Simple Tree',
						// width: 200,
						// height: 150,
						// store: store,
						rootVisible: false
					}]
				},{
			    	enableTabScroll:true,//能够滚动收缩
					activeTab:0,//激活第一个标签
			        region: 'center',     // center region is required, no width/height specified
			        xtype: 'tabpanel',
			        layout: 'fit',
			        // margin: '5 5 0 0',
			        items:[{
			        	id:'homePage',
			        	layout:'fit',
			        	title:'首页',
			        	autoScroll:true,
			        	closable:true,
			        	html:'<iframe scrolling="auto" frameborder="0" width="100%" height="630" src="Welecome.jsp"></iframe>'
			        }]
			    }]},
			    {
		        region: 'north',     // position for region
		        xtype: 'container',
		        height: 50,
		        // split: true, 
		        collapsible: true,          
		        margin: '0'
		    }]
		});
}
});
var store = Ext.create('Ext.data.TreeStore',{
    root: {
        expanded: true,
        children: [
            { text: '课程模块', leaf: true },
            { text: 'homework', expanded: true, children: [
                { text: 'book report', leaf: true },
                { text: 'algebra', leaf: true}
            ] },
            { text: 'buy lottery tickets', leaf: true }
        ]
    }
});
