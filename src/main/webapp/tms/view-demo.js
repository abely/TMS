Ext.onReady(function() {
	Ext.create('Ext.container.Viewport', {
		layout : 'border',
		defaults : {
			autoScroll : true
		},
		items : [
		{ 
			region : 'north', 
			html : ' <div text-align:center; region="north" split="true" border="false" style="overflow: hidden; height: 30px;line-height:30px;background: #157FCC repeat-x center 50%; color: #fff; font-family: Verdana, 微软雅黑,黑体"><span style="float: right; padding-right: 20px;" class="head">欢迎您使用！ <font color="red"><%=Session["Roles_Name"]%></font>&nbsp;&nbsp;<font color="red"><%=Session["RealName"]%></font>&nbsp;&nbsp;<a href="#" id="editpass" onclick="modifyPwd()"><font color="white">修改密码</font></a> <a href="#" id="loginOut" onclick="logOut()"><font color="white">安全退出</font></a></span> <span style="padding-left: 10px;font-size: 16px;"><img src="UIJS/Images/blocks.gif" width="20" height="20" align="absmiddle" /><font color="white">仓库管理系统</font></span></div>', 
			border : false, margin : '0 0 5 0' 
		}, 
		{
			region : 'west',
			//collapsible : true,
			title : '管理菜单',
			width : 200,
			layout:'fit',
			items:[tree]

			// could use a TreePanel or AccordionLayout for navigational

			// items
		},

	/*

	* { region : 'south', title : 'South Panel', collapsible :

	* true, //html : 'Information goes here', split : true, height :

	* 100, minHeight : 100 },

	*/ 

	/*

	* { region : 'east', title : 'East Panel', collapsible : true,

	* split : true, width : 150 },

	*/

	{

		region : 'center',

		animate: true, //动画效果启用 

		layout:'fit',

		//layout:'border',

		items:[contentPanel]

		/*

		xtype : 'tabpanel', // TabPanel itself has no title

		activeTab : 0, // First tab active by default

		itemId: 'center-window',

		id:'center-window',

		animate: true, //动画效果启用 


		items : {

		title : '首页',

		//html : 'The first tab\'s content. Others may be added dynamically'

		//autoLoad:'register.jsp'//只有一个基本的panel 

		html:'<iframe scrolling="auto" frameborder="0" width="100%" height="100%" src="register.jsp"></iframe>' 

		}

		*/

	}

	]

});

});
var contentPanel=new Ext.TabPanel({

	id:'centerP',

	enableTabScroll:true,//能够滚动收缩

	activeTab:0,//激活第一个标签

	items:[{
		id:'homePage',
		layout:'fit',
		title:'首页',
		autoScroll:true,
		html:'<iframe scrolling="auto" frameborder="0" width="100%" height="630" src="Welecome.jsp"></iframe>'
	}]

});

var store = Ext.create('Ext.data.TreeStore', {
	root : {
		expanded : true,
		children : [{
			text : "基本信息管理",
		// href:"http://www.easyjf.com",
		// leaf : true
		expanded : true,
		children : [{
			text : "库位管理",

			url:'index.jsp',

			leaf : true

		}, {

			text : "货物管理",

			url:'Users.jsp',

			leaf : true

		}]

	}, {

		text : "系统信息管理",

		expanded : true,

		children : [{

			text : "用户管理",

			url:'UIJS/UserManage/html/User.html',

			leaf : true

		}, {

			text : "权限管理",

			url:'Users.jsp',

			leaf : true

		}]

	}]

}

});



var tree=Ext.create('Ext.tree.Panel', {

			// title : 'Simple Tree',

			// width : 500,

			// height : auto,

			useArrows : true,//展开按钮图标是箭头还是+-

			store : store,

			rootVisible : false

			// renderTo : Ext.getBody()
		});
tree.on("itemclick", treeClick);
function treeClick(view, record, item, index, e) {

	var text = record.get('text');// 获取tree的叶子节点的值

	var leaf = record.get('leaf');

	var url= record.get('url');

	//alert(url);

	// var tabPanel = Ext.getCmp('center-window');//获取tabpanel的id

	var tabPage = contentPanel.getComponent(text);// 判断面板中是否存在该页面

	if (leaf){	// 判断是否是叶子节点
		if (!tabPage) {
			var id = text;
			var index = contentPanel.items.length + 1;
			tabPage = contentPanel.add({// 动态添加tab页
			// tabPanel.add({//动态添加tab页
				"id" : 'demo',
				title : text,
				//layout:'fit',
				//html : '<iframe scrolling="no" frameborder="0" width="100%" height="620" src="Users.jsp"></iframe>',
				html : '<iframe scrolling="no" frameborder="0" width="100%" height="100%" src='+url+'></iframe>',
				closable : true// 允许关闭
			})
			contentPanel.setActiveTab(tabPage);// 激活当前tab页
		} else {
			contentPanel.setActiveTab(tabPage);// 激活当前tab页
		}

	}

}