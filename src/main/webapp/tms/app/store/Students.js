Ext.define('StudentModel',{
	extend:'Ext.data.Model',
	fields :[{
		name:'id',
		type:'int'
	},{
		name:'employeeId',
		type:'string'
	},{
		name:'cnName',
		type:'string'
	}]
})

Ext.define('TMS.store.Students',{
	extend:'Ext.data.Store',
	model:'StudentModel',
	alias:'store.students'
	/*proxy:{
		type:'ajax',
		api:{
			read:'person',
			update:''
		},
		reader:{
			type:'json',
			rootProperty:'personnels'
		}
	},*/
	// autoLoad:true
})