Ext.define('TMS.sotre.MenuViewStore',{
	extend:'Ext.data.TreeStore',
	storeId:'MenuViewStore',
	root: {
		expanded: true,
		children: [
		{ 
			text: 'TrainRecord', 
			expanded: false,
			children:[{
				name:'menuviewlist',
				text : "库位管理",
				url:'index.jsp',
				leaf : true
			}] 
		},{ 
			text: 'homework',
			expanded: false,
			children: [{ 
				text: 'book report', 
				leaf: true 
			},{ 
				text: 'algebra', 
				leaf: true
			}] 
		},{ 
			text: 'buy lottery tickets', 
			leaf: true
		}
		]
	}
});