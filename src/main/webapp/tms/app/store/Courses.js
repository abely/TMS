Ext.define('CourseModel',{
	extend:'Ext.data.Model',
	fields:[{
		name:'courseId',
		type:'string'
	},{
		name:'courseName',
		type:'string'
	},{
		name:'courseClassic',
		type:'string'
	},{
		name:'courseDesc',
		type:'string'
	},{
		name:'createDate',
		type:'date',
		dateFormat: 'Y-m-d'
	}]
});

Ext.define('TMS.store.Courses',{
	extend:'Ext.data.Store',
	model:'CourseModel',
	alias:'store.courses',
	//storeId : 'C_courseStore',//
	pageSize :20,
	proxy: {
		type: 'ajax',
		url: 'getAllCourses',
		reader: {
			type: 'json',
			rootProperty: 'courses'
		}
	}
	//autoLoad:true
});
