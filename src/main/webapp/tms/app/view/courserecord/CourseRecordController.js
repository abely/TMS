Ext.define('TMS.view.courserecord.CourseRecordController',{
	extend:'Ext.app.ViewController',
	alias:'controller.courserecordview',
	add:function(){
		var win = Ext.create('CourseRecordAdd');
	},
	select:function(combo, record, eOpts){
		var param =record.get('searchParam'),
			courseNum = Ext.getCmp('CR_courseNum'),
			courseDetail= Ext.getCmp('CR_courseDetail'),
			teacherID = Ext.getCmp('CR_courseTeacherID'),
			teacherName = Ext.getCmp('CR_courseTeacherName'),
			courseType = Ext.getCmp('courseType'),
			startDate = Ext.getCmp('CR_starttime'),
			endTDate = Ext.getCmp('CR_endtime');
		
		if(param==teacherID.fieldLabel){
			teacherID.show();
		}
		if(param==courseDetail.fieldLabel){
			courseDetail.show();
		}
	}
});