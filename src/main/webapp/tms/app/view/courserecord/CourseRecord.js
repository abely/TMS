Ext.Loader.setConfig({enabled: true});
Ext.Loader.setPath('Ext.ux.grid', 'extjs/ux/classic/src/grid/');



Ext.define('CourseRecordAdd', {
	extend : 'Ext.window.Window',
	title : '添加记录',
	layout : 'fit',
	modal:true,
	id : 'CR_AddRecordWin',
	autoShow : true,
	width:700,
	height:300,
	initComponent : function() {
		this.items = [ {
			id : 'CR_AddRecordForm',
			xtype : 'form',
			margin:'20px 20px 10px 50px',
			border : false,
			defaults:{
				margin:'5px 10px 0 0',
				width:300
			},  
			layout: {
				type: 'table',
		        columns: 2
		    },
		    items : [ {
		    	id : 'CR_CourseIdText',
		    	xtype : 'textfield',
		    	fieldLabel : '课程编号',
		    	name : 'courseId'
		    },{
		    	id : 'CR_CourseNameText',
		    	xtype : 'textfield',
		    	fieldLabel : '课题名称',
		    	name : 'courseName'
		    },{
		    	id : 'CR_CreateDateText',
		    	xtype : 'datefield',
		    	fieldLabel : '培训时间',
		    	name : 'cr_eateDate',
		    	format :'Y-m-d',
				value :new Date()
			} , {
				id : 'CR_ClassicCombo',
				xtype : 'combobox',
				fieldLabel : '培训类型',
				name : 'courseClassic'
			},{
				id : 'CR_teacherText',
				xtype : 'textfield',
				fieldLabel : '培训讲师',
				name : 'teacher'
			},{
				id : 'CR_SearchTeacherBtn',//delete this component
				xtype : 'button',
				text:'查询',
				margin:'0 0 0 15px',
				width:50
			},{
				id : 'CR_CourseDescText',
				xtype : 'textarea',
				fieldLabel : '课题描述',
				name : 'courseDesc'
			}]
		} ];
		this.buttons = [ {
			id : 'CR_SaveRecordBtn',
			text : '保存',
			action : 'save'
		}, {
			id : 'CR_CancelRecordBtn',
			text:'取消',
			scope:this,
			handler:this.close
		} ];
		this.callParent();
	}
});

var searchParam = Ext.create('Ext.data.Store', {
	fields: ['searchParam'],
	data : [{
		"searchParam":"课程编号"
	},{
		"searchParam":"上课日期"
	},{
		"searchParam":"课题明细"
	},{
		"searchParam":"讲师ID"
	},{
		"searchParam":"讲师姓名"
	},{
		"searchParam":"培训类型"
	}]

});

var courseType = Ext.create('Ext.data.Store', {
	fields: ['courseType'],
	data : [{
		"courseType":"内部培训"
	},{
		"courseType":"外部培训"
	}]

});

Ext.define('Override.form.field.VTypes', {
	override: 'Ext.form.field.VTypes',
    num: function(value) {
    	return this.numRe.test(value);
    },
    numRe: /^[a-zA-z][0-9]*$/i,
    numText: 'Worng Input! example("A001"or"a001")',
    numMask: /[\w]/i
});
var recordColumns = [ {
				xtype: 'rownumberer'
			}, {
				header : '课程编号',
				dataIndex : 'courseId',
				flex : 1
			}, {
				header : '课程名称',
				dataIndex : 'courseName',
				flex : 1
			},{
				header : '课程分类',
				dataIndex : 'courseClassic',
				flex : 1
			},{
				header : '课程描述',
				dataIndex : 'courseDesc',
				flex : 1
			},{
				header : '创建时间',
				dataIndex : 'cr_eateDate',
				flex : 1
			},{
				xtype:'actioncolumn',
				width:'10%',
				text:'编辑 删除',
				items: [{
					iconCls: 'edit',  
					tooltip: 'Edit'
				},'-',{
					iconCls: 'delete',
					tooltip: 'Delete'
				}]
			} ];
Ext.define('TMS.view.courserecord.CourseRecord',{
	extend:'Ext.panel.Panel',
	requires:['TMS.view.courserecord.CourseRecordController','TMS.view.courserecord.CourseRecordModel',
	'Ext.ux.grid.SubTable'],
	controller:'courserecordview',
	viewModel:'courserecordview',
	alias:'widget.courserecordview',
	id : 'CR_MainPanel',
	// layout:'fit',
	height:600,
	//title:'Corse Maintanence',
	frame: true,
	autoScr_oll:true,
	items :[{
		title:'Record List',
		id : 'CR_SearchRecordForm',	
		xtype:'form',
		height:100,
		layout:'hbox',
		defaults:{
			margin:'10px'
		},
		items: [{
			id : 'CR_SearchTypeCombo',
			xtype:'combobox',
			emptyText :'请选择查询方式...',
			store:searchParam,
			displayField: 'searchParam',
			listeners:{
				select:'select'
			}
		},{
			id:'CR_courseNum',
			xtype:'textfield',
			emptyText:'默认按课程编号查询...'	,
			fieldLabel:'课程编号',
			labelWidth:70,
			vtype:'num',
			msgTarget:'side'
		},{
			id:'CR_courseDetail',
			xtype:'textfield',
			fieldLabel:'课题明细',
			labelWidth:70,
			hidden:true
		},{
			id:'CR_courseTeacherID',
			xtype:'textfield',
			fieldLabel:'讲师ID',
			labelWidth:70,
			hidden:true
		},{
			id:'CR_courseTeacherName',
			xtype:'textfield',
			fieldLabel:'讲师姓名',
			labelWidth:70,
			hidden:true
		},{
			id:'courseType',
			xtype:'combobox',
			fieldLabel:'培训类型',
			labelWidth:70,
			store:courseType,
			displayField:'courseType',
			emptyText:'请选择培训类型...',
			hidden:true
		},{
			id:'CR_starttime',
			xtype:'datefield',
			fieldLabel:'Start Time',
			labelWidth:70,
			hidden : true
		},{
			id:'CR_endtime',
			xtype:'datefield',
			fieldLabel:'End Time',
			labelWidth:70,
			hidden : true
		},{
			xtype:'button',
			iconCls:'search',
			text:'查询',
			// listeners:{
			// }
		}]
	},{
		xtype:'grid',
		height:470,
		store : {
			type:'courses'
		},
		tbar:[{
			xtype:'button',
			iconCls:'add',
			text:'添加',
			listeners:{
				click:'add'
			}
		}],
			/*plugins: {
			ptype: 'subtable',
			association: 'students',
			headerWidth: 32,
			columns: [{
				text: 'EmployeeID',
				dataIndex: 'employeeId',
				width: 100
			},{
				width: 120,
				text: 'CnName',
				dataIndex: 'cnName'
			}]
		},*/
		columns:recordColumns,
		dockedItems: [{
			xtype: 'pagingtoolbar',
			store: 'Courses',
			dock: 'bottom',
			displayInfo: true
		}]
	}]
});


