Ext.define('TMS.view.menuview.MenuViewController',{
	extend:'Ext.app.ViewController',
	// requires:['TMS.view.course.CourseView'],
	alias : 'controller.menuview',
	views:['menuview.MenuviewList','menuview.CourseView'],
	treeClick:function( view, record, item, index, e){
		var text = record.get('text');// 获取tree的叶子节点的值
		var name =record.get('name');
		var leaf = record.get('leaf');

		var url= record.get('url');
		// var panel = Ext.widget('menuview').up('container');
		// alert(panel);
		var contentPanel = view.up('container').up('container').up('container').down('tabpanel');
		// alert(contentPanel);
		var tabPage = contentPanel.getComponent(name);// 判断面板中是否存在该页面
		var win1 =  Ext.getCmp('addRecordWindow'),
		win2 = Ext.getCmp('addWindow');
		if (leaf){	// 判断是否是叶子节点
			if (!tabPage) {
				
				var index = contentPanel.items.length + 1;
				tabPage = contentPanel.add({// 动态添加tab页
					id : name,
					title : text,
					items:[{
						xtype:name
					}],
					closable : true// 允许关闭
				})
				if(win1||win2){
					Ext.Msg.alert("提示","请先保存当前记录!");
				}else{
					contentPanel.setActiveTab(tabPage);// 激活当前tab页
				}
				
			} else {
				if(win1||win2){
					Ext.Msg.alert("提示","请先保存当前记录!");
				}else{
					contentPanel.setActiveTab(tabPage);// 激活当前tab页
				}
				
			}

		}
	}
});