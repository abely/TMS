Ext.define('TMS.view.menuview.MenuView',{
	extend:'Ext.tree.Panel',
	requires:['TMS.view.menuview.MenuViewController','TMS.view.menuview.MenuViewModel'],
	alias:'widget.menuview',
	rootVisible: false,
	controller :'menuview',
	viewModel:'menuview',
	// store:'MenuViewStore',
	initComponent: function(){
		this.store={
			root: {
				expanded: true,
				children: [
				{ 
					text: '课程培训模块', 
					expanded: false,
					children:[{
						name:'courseview',
						text : "课程管理",
						leaf : true
					},{
						name:'courserecordview',
						text : '课程记录管理',
						leaf : true
					}] 
				},{ 
					text: '上岗认证模块',
					expanded: false,
					children: [{ 
						text: 'book report', 
						leaf: true 
					},{ 
						text: 'algebra', 
						leaf: true
					}] 
				}
				]
			}
		};
		this.callParent();
	},
	listeners :{
		itemdblclick:'treeClick'	
	}
});