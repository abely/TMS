Ext.define('TMS.view.menuview.MenuviewList', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.menuviewlist',
	title : 'All Users',
	store : 'Personnels',
	
	autoShow:true,
	initComponent : function() {
		this.columns = [ {
			header : 'ID',
			dataIndex : 'id',
			flex : 1
		}, {
			header : 'Name',
			dataIndex : 'employeeId',
			flex : 1
		}, {
			header : 'Conpany',
			dataIndex : 'companyId',
			flex : 1
		},
		{
			header : 'CN',
			dataIndex : 'cnName',
			flex : 1
		},{
			header : 'Department',
			dataIndex : 'department',
			flex : 1
		},{
			header : 'Section',
			dataIndex : 'section',
			flex : 1
		},{
			header : 'Position',
			dataIndex : 'positionName',
			flex : 1
		},{
			header : 'SuperiorName',
			dataIndex : 'superiorName',
			flex : 1
		},{
			header : 'RemartDesc',
			dataIndex : 'remarkDesc',
			flex : 1
		},{
			header : 'EmClassic',
			dataIndex : 'emClassic',
			flex : 1
		} ];
		this.callParent();
	}
});