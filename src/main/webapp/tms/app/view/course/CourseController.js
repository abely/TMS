Ext.define('TMS.view.course.CourseController',{
	extend:'Ext.app.ViewController',
	//requires:['TMS.view.course.CourseAdd'],
	alias : 'controller.courseview',
	//views:['course.CourseAdd'],
	add : function(g){
		var win = Ext.create('CourseAdd');
		win.show();
	},
	select:function(combo, record, eOpts){
		var param =record.get('searchParam');
		var start = Ext.getCmp('CV_starttime');
		var end = Ext.getCmp('CV_endtime');
		var name = Ext.getCmp('CV_searchName');
		var num = Ext.getCmp('CV_searchNum');
		
		if(param=="创建时间"){
			num.hide();
			name.hide();
			start.show();
			end.show();

		}else if(param=="课程名称"){
			name.show();
			start.hide();
			end.hide();
			num.hide();
		}else if(param=="课程编号"){
			name.hide();
			start.hide();
			end.hide();
			num.show();
		}
	},
	searchAction:function(){
		var searchType =Ext.getCmp('CV_searchType');
		var start = Ext.getCmp('CV_starttime');
		var end = Ext.getCmp('CV_endtime');
		var name = Ext.getCmp('CV_searchName');
		var num = Ext.getCmp('CV_searchNum');
		if(searchType.value=="课程编号"){
			alert(num.value);
		}else if(searchType.value=="创建时间"){
			if(start.value!=null&&end.value!=null){
				alert(start.value+" "+end.value);
			}else{
				Ext.Msg.alert('提示','时间不能为空');
			}
			
		}
	},
	saveCourse : function(){

	},
	listCourses : function(){
		var store = Ext.getStore('Courses');
		store.load();
	}
});