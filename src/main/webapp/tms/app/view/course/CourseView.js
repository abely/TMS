(function(){
    var searchParam = Ext.create('Ext.data.Store', {
    	fields: ['searchParam'],
    	data : [{
    		"searchParam":"课程编号"
    	},{
    		"searchParam":"课程名称"
    	},{
    		"searchParam":"创建时间"
    	}]

    });

    var courseType=Ext.create('Ext.data.Store',{
      fields: ['typeName'],
      data : [{
        "typeName":"A-培训引导类"
      },{
        "typeName":"B-安全引导类"
      },{
        "typeName":"C-培训引导类"
      },{
        "typeName":"D-呵呵引导类"
      },{
        "typeName":"F-呵呵引导类"
      },{
        "typeName":"G-哈哈引导类"
      },{
        "typeName":"H-哈哈引导类"
      }]
    });
    Ext.define('Override.form.field.VTypes', {
    	override: 'Ext.form.field.VTypes',

        num: function(value) {
        	return this.numRe.test(value);
        },
        numRe: /^[a-zA-z][0-9]*$/i,
        numText: 'Worng Input! example("A001"or"a001")',
        numMask: /[\w]/i
      });

    Ext.define('CourseAdd', {
      extend : 'Ext.window.Window',
      title : 'Add Course',
      layout : 'fit',
      id :'CV_AddCourseWin',
      autoShow : true,
      modal:true,
      border : false,
      width:500,
      height:300,
      initComponent : function() {
        this.items = [ {
          xtype : 'form',
          id:'CV_AddCourseForm',
          margin:'20px 20px 10px 50px',
          border : false,
          defaults:{
            width:370
          },
          items : [ {
            id:'CV_CourseIdText',
            xtype : 'textfield',
            fieldLabel : '课程编号',
            name : 'courseId',
            editable :false,
            emptyText:'编号自动生成'
          },{
            id:'CV_CourseNameText',
            xtype : 'textfield',
            fieldLabel : '课程名称',
            name : 'courseName'
          }, {
            id:'CV_ClassicCombo',
            xtype : 'combobox',
            fieldLabel : '课程分类',
            name : 'courseClassic',
            store: courseType,
            displayField: 'typeName',
            listeners: {
              select :  function(combo,record,obj){
                var courseType = combo.getValue();
                Ext.Ajax.request({
                  url:'generateCourseId',
                  method:'POST',
                  params : {
                    courseType:courseType
                  },
                  success : function(resp){
                    Ext.getCmp('CV_CourseIdText').setValue(resp.responseText);
                  }
                });
              }
            }
          },{
            id:'CV_CourseDescText',
            xtype : 'textarea',
            fieldLabel : '课程描述',
            name : 'courseDesc'
          },{
             id:'CV_CreateDateText',
            xtype : 'datefield',
            fieldLabel : '创建时间',
            name : 'createDate',
            //formatText :'yyyy-MM-dd',
            value :new Date()
          }  ]
        } ];
        this.buttons = [ {
          id:'CV_SaveCourseBtn',
          text : '保存',
          action : 'saveCourse'
        }, {
          id:'CV_CancelCourseBtn',
          text:'取消',
          scope:this,
          handler:this.close
        } ];
        this.callParent();
      }
    });
    var courseColumns = [  {
        header : '课程编号',
        dataIndex : 'courseId',
        flex : 1
      }, {
        header : '课程名称',
        dataIndex : 'courseName',
        flex : 1
      },
      {
        header : '课程分类',
        dataIndex : 'courseClassic',
        flex : 1
      },{
        header : '课程描述',
        dataIndex : 'courseDesc',
        flex : 1
      },{
        header : '创建时间',
        dataIndex : 'createDate',
       /* flex : 1,
        xtype: 'datecolumn', 
        format:'Y-m-d' ,*/
        renderer: Ext.util.Format.dateRenderer('Y-m-d')
     },{
        xtype:'actioncolumn',
        width:'10%',
        text:'编辑 删除',
        items: [{
          iconCls: 'edit',
          tooltip: 'Edit'
        },'-',{
          iconCls: 'delete',
          tooltip: 'Delete'
        }]
      } ];
    Ext.define('TMS.view.course.CourseView',{
        extend:'Ext.panel.Panel',
        requires:['TMS.view.course.CourseController','Ext.grid.selection.SpreadsheetModel',
        'Ext.grid.plugin.Clipboard'],
        controller:'courseview',
        alias:'widget.courseview',
        xtype:'courseview',
        id:'CV_MainPanel',
        margin:'5 5 5 5',
        titleCollapse : true,
        frame: true,
        height:600,
        items:[{
            title:'Course List',
            xtype:'form',
            id:'CV_SearchFormId',
            height:100,
            layout:'hbox',
            defaults:{
              margin:'20px'
            },
            items:[{
             id:'CV_searchType',
             xtype:'combobox',
             emptyText :'请选择查询方式...',
             store:searchParam,
             displayField: 'searchParam',
             listeners:{
               select:'select'
             }
           },{
             id:'CV_searchNum',
             xtype:'textfield',
             emptyText:'默认按课程编号查询...' ,
             fieldLabel:'课程编号',
             labelWidth:70,
             vtype:'num',
             msgTarget:'side'
           },{
             id:'CV_searchName',
             xtype:'textfield',
             fieldLabel:'课程名称',
             labelWidth:70,
             hidden:true
           },{
             id:'CV_starttime',
             xtype:'datefield',
             fieldLabel:'Start Time',
             labelWidth:70,
             hidden : true,
             format:'Y-m-d',
             dateformat:'Y-m-d'
           },{
             id:'CV_endtime',
             xtype:'datefield',
             fieldLabel:'End Time',
             labelWidth:70,
             hidden : true,
             format:'Y-m-d',
             dateformat:'Y-m-d'
           },{
             id:'CV_SearchCourseBtn',
             xtype:'button',
             iconCls:'search',
             text:'查询',
             listeners:{
              click:'searchAction'
            }
          }]
        },{
          id:'CV_CourseGrid',
          xtype:'grid',
          height:460,
          listeners:{
            afterrender : 'listCourses'
          },
          store : 'Courses',
          tbar:[{
            id:'CV_AddCourseBtn',
            xtype:'button',
            iconCls:'add',
            text:'添加',
            listeners : {
              click : 'add'
            }
          },'-',{
            id:'CV_DeleteCourseBtn',
            xtype:'button',
            iconCls:'delete',
            text:'删除',
            listeners : {
             // click : 'delete'
            }
          }],
          forceFit: true,
          autoShow:true,
          columns:courseColumns,
          dockedItems: [{
            xtype: 'pagingtoolbar',
            store: 'Courses',
            dock: 'bottom',
            displayInfo: true
          }]
        }]
      });

}());
