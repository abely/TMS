package com.sanmina.tms.dao;

import com.sanmina.tms.domain.TrainTeacher;

import java.util.List;

public interface TrainTeacherMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TrainTeacher record);

    int insertSelective(TrainTeacher record);

    TrainTeacher selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TrainTeacher record);

    int updateByPrimaryKey(TrainTeacher record);

    List<TrainTeacher> findAllTrainTeacher();

    List<TrainTeacher> findAllTrainTeacher(int start, int limit);
}