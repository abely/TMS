package com.sanmina.tms.dao;

import com.sanmina.tms.domain.TrainCourse;
import org.springframework.stereotype.Repository;

import java.util.List;
public interface TrainCourseMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TrainCourse record);

    int insertSelective(TrainCourse record);

    TrainCourse selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TrainCourse record);

    int updateByPrimaryKey(TrainCourse record);

    List<TrainCourse> findAllCourse();

    List<TrainCourse> findAllCourse(int start, int limit);

    List<TrainCourse> findCourseByCourseClassic(String courseClassic);
}
