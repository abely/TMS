package com.sanmina.tms.dao;

import com.sanmina.tms.domain.Person;

import java.util.List;

public interface PersonMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Person record);

    int insertSelective(Person record);

    Person selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Person record);

    int updateByPrimaryKey(Person record);

    List<Person> findAllPerson(int start, int limit);

    List<Person> findAllPerson();

}