package com.sanmina.tms.dao;

import com.sanmina.tms.domain.TrainStudent;

import java.util.List;

public interface TrainStudentMapper {
    int deleteByPrimaryKey(Integer refId);

    int insert(TrainStudent record);

    int insertSelective(TrainStudent record);

    TrainStudent selectByPrimaryKey(Integer refId);

    int updateByPrimaryKeySelective(TrainStudent record);

    int updateByPrimaryKey(TrainStudent record);

    List<TrainStudent> findAllTrainStudent(int start, int limit);

    List<TrainStudent> findAllTrainStudent();
}