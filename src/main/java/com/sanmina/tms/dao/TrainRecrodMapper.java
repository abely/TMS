package com.sanmina.tms.dao;

import com.sanmina.tms.domain.TrainRecrod;

import java.util.List;

public interface TrainRecrodMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TrainRecrod record);

    int insertSelective(TrainRecrod record);

    TrainRecrod selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TrainRecrod record);

    int updateByPrimaryKey(TrainRecrod record);

    List<TrainRecrod> findAllTrainRecrod(int start, int limit);

    List<TrainRecrod> findAllTrainRecrod();
}