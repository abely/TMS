package com.sanmina.tms.controller.ajax;

import com.sanmina.tms.domain.TrainStudent;
import com.sanmina.tms.server.TrainStudentServer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by abely on 16-5-6.
 */
public class StudentAction {

    @Resource
    private TrainStudentServer trainStudentServer;

    public List<TrainStudent> getAllTrainStudent(){
        return trainStudentServer.getAllTrainStudent();
    }

    @RequestMapping(value = "getAllStudent" ,produces = {"application/json;charset=UTF-8"})
    public List<TrainStudent> getAllTrainStudent(@RequestParam("start")int start, @RequestParam("limit")int limit){
        return trainStudentServer.getAllTrainStudent(start,limit);
    }
}
