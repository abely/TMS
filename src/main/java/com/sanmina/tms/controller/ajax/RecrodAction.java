package com.sanmina.tms.controller.ajax;

import com.sanmina.tms.domain.TrainRecrod;
import com.sanmina.tms.server.TrainRecrodServer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by abely on 16-5-6.
 */
public class RecrodAction {

    @Resource
    private TrainRecrodServer trainRecrodServer;

    public List<TrainRecrod> getAllTrainRecrod(){
        return trainRecrodServer.getAllTrainRecrod();
    }

    @RequestMapping(value = "getAllRecrod" ,produces = {"application/json;charset=UTF-8"})
    public List<TrainRecrod> getAllTrainRecrod(@RequestParam("start")int start, @RequestParam("limit")int limit){
        return trainRecrodServer.getAllTrainRecrod(start, limit);
    }
}
