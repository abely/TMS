package com.sanmina.tms.controller.ajax;

import com.sanmina.tms.domain.Person;
import com.sanmina.tms.server.StaffServer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by abely on 16-5-6.
 */
public class StaffAction {

    @Resource
    private StaffServer staffServer;

    public List<Person> getAllStaff(){
        return staffServer.getAllStaff();
    }

    @RequestMapping(value = "getAllStaff" ,produces = {"application/json;charset=UTF-8"})
    public List<Person> getAllStaff(@RequestParam("start")int start, @RequestParam("limit")int limit){
        return staffServer.getAllStaff(start, limit);
    }
}
