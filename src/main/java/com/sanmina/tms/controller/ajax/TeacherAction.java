package com.sanmina.tms.controller.ajax;

import com.sanmina.tms.domain.TrainTeacher;
import com.sanmina.tms.server.TrainTeacherServer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by abely on 16-5-6.
 */
public class TeacherAction {

    @Resource
    private TrainTeacherServer trainTeacherServer;

    public List<TrainTeacher> getAllTrainTeacher(){
        return trainTeacherServer.getAllTrainTeacher();
    }

    @RequestMapping(value = "getAllTrainTeacher" ,produces = {"application/json;charset=UTF-8"})
    public List<TrainTeacher> getAllTrainTeacher(@RequestParam("start")int start, @RequestParam("limit")int limit){
        return trainTeacherServer.getAllTrainTeacher(start,limit);
    }
}
