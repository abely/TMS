package com.sanmina.tms.controller.ajax;

import com.sanmina.tms.domain.TrainCourse;
import com.sanmina.tms.server.TrainCourseServer;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.inject.Inject;
import java.util.*;

/**
 * Created by abely on 16-5-5.
 */

@RestController
public class CourseAction {

    @Resource
    TrainCourseServer trainCourseServer;

    @RequestMapping(value = "getAllCourses", produces = {"application/json;charset=UTF-8"})
    public List<TrainCourse> getAllCourse(@RequestParam("start") int start, @RequestParam("limit") int limit) {
        return trainCourseServer.getAllTrainCourse(start, limit);
    }

    /**
     * Auto generate CourseId
     * @param courseType
     * @return
     */
    @RequestMapping(value = "generateCourseId", produces = {"application/json;charset=UTF-8"})
    public String generateCourseId(@RequestParam("courseType") String courseType) {
        String courseId = trainCourseServer.generateCourseId(courseType);
        return courseId;
    }

    public void addCourse(){

    }
}
