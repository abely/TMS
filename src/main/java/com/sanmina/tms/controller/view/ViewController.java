package com.sanmina.tms.controller.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by abely on 16-5-5.
 */
@Controller
public class ViewController {

    Logger logger = LoggerFactory.getLogger(ViewController.class);

    @RequestMapping(value = "/")
    public String toIndex(){
        return "index";
    }

    @RequestMapping(value="/mylogin")
    public String toLogin(){
        return "login";
    }

}
