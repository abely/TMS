package com.sanmina.tms.domain;

public class Person {
    private Integer id;

    private String employeeId="";

    private String companyId="";

    private String cnName="";

    private String department="";

    private String section="";

    private String positionName="";

    private String superiorName="";

    private String remarkDesc="";

    private String emClassic="";

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId == null ? null : employeeId.trim();
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId == null ? null : companyId.trim();
    }

    public String getCnName() {
        return cnName;
    }

    public void setCnName(String cnName) {
        this.cnName = cnName == null ? null : cnName.trim();
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department == null ? null : department.trim();
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section == null ? null : section.trim();
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName == null ? null : positionName.trim();
    }

    public String getSuperiorName() {
        return superiorName;
    }

    public void setSuperiorName(String superiorName) {
        this.superiorName = superiorName == null ? null : superiorName.trim();
    }

    public String getRemarkDesc() {
        return remarkDesc;
    }

    public void setRemarkDesc(String remarkDesc) {
        this.remarkDesc = remarkDesc == null ? null : remarkDesc.trim();
    }

    public String getEmClassic() {
        return emClassic;
    }

    public void setEmClassic(String emClassic) {
        this.emClassic = emClassic == null ? null : emClassic.trim();
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", employeeId='" + employeeId + '\'' +
                ", companyId='" + companyId + '\'' +
                ", cnName='" + cnName + '\'' +
                ", department='" + department + '\'' +
                ", section='" + section + '\'' +
                ", positionName='" + positionName + '\'' +
                ", superiorName='" + superiorName + '\'' +
                ", remarkDesc='" + remarkDesc + '\'' +
                ", emClassic='" + emClassic + '\'' +
                '}';
    }
}