package com.sanmina.tms.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class TrainCourse {
    private Integer id;

    private String courseId;

    private String courseName;

    private String courseClassic;

    private String courseDesc;

    private Date createDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId == null ? null : courseId.trim();
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName == null ? null : courseName.trim();
    }

    public String getCourseClassic() {
        return courseClassic;
    }

    public void setCourseClassic(String courseClassic) {
        this.courseClassic = courseClassic == null ? null : courseClassic.trim();
    }

    public String getCourseDesc() {
        return courseDesc;
    }

    public void setCourseDesc(String courseDesc) {
        this.courseDesc = courseDesc == null ? null : courseDesc.trim();
    }

    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}