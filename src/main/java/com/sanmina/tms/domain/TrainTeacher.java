package com.sanmina.tms.domain;

public class TrainTeacher {
    private Integer id;

    private String teacherType="";

    private String outTeachName="";

    private String outTeachCompany="";

    private String outTrainMoney="";

    private String outTrainPhone="";

    private String trainDesc="";

    private String emId="";

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTeacherType() {
        return teacherType;
    }

    public void setTeacherType(String teacherType) {
        this.teacherType = teacherType == null ? null : teacherType.trim();
    }

    public String getOutTeachName() {
        return outTeachName;
    }

    public void setOutTeachName(String outTeachName) {
        this.outTeachName = outTeachName == null ? null : outTeachName.trim();
    }

    public String getOutTeachCompany() {
        return outTeachCompany;
    }

    public void setOutTeachCompany(String outTeachCompany) {
        this.outTeachCompany = outTeachCompany == null ? null : outTeachCompany.trim();
    }

    public String getOutTrainMoney() {
        return outTrainMoney;
    }

    public void setOutTrainMoney(String outTrainMoney) {
        this.outTrainMoney = outTrainMoney == null ? null : outTrainMoney.trim();
    }

    public String getOutTrainPhone() {
        return outTrainPhone;
    }

    public void setOutTrainPhone(String outTrainPhone) {
        this.outTrainPhone = outTrainPhone == null ? null : outTrainPhone.trim();
    }

    public String getTrainDesc() {
        return trainDesc;
    }

    public void setTrainDesc(String trainDesc) {
        this.trainDesc = trainDesc == null ? null : trainDesc.trim();
    }

    public String getEmId() {
        return emId;
    }

    public void setEmId(String emId) {
        this.emId = emId == null ? null : emId.trim();
    }
}