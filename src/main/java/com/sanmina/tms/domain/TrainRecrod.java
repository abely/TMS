package com.sanmina.tms.domain;

import java.util.Date;

public class TrainRecrod {
    private Integer id;

    private String courseId="";

    private String courseDetail="";

    private Date trainDate;

    private String trainType="";

    private Integer trainTeachId=0;

    private Long stuCount=0L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId == null ? null : courseId.trim();
    }

    public String getCourseDetail() {
        return courseDetail;
    }

    public void setCourseDetail(String courseDetail) {
        this.courseDetail = courseDetail == null ? null : courseDetail.trim();
    }

    public Date getTrainDate() {
        return trainDate;
    }

    public void setTrainDate(Date trainDate) {
        this.trainDate = trainDate;
    }

    public String getTrainType() {
        return trainType;
    }

    public void setTrainType(String trainType) {
        this.trainType = trainType == null ? null : trainType.trim();
    }

    public Integer getTrainTeachId() {
        return trainTeachId;
    }

    public void setTrainTeachId(Integer trainTeachId) {
        this.trainTeachId = trainTeachId;
    }

    public Long getStuCount() {
        return stuCount;
    }

    public void setStuCount(Long stuCount) {
        this.stuCount = stuCount;
    }
}