package com.sanmina.tms.test;

import com.sanmina.tms.dao.*;
import com.sanmina.tms.domain.*;
import com.sanmina.tms.server.TrainCourseServer;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by abely on 16-2-24.
 */
@RunWith(SpringJUnit4ClassRunner.class)     //表示继承了SpringJUnit4ClassRunner类
@ContextConfiguration(locations = {"classpath:config/spring-mybatis.xml"})
public class Test {
    @Resource
    TrainTeacherMapper trainTeacherMapper;
    @Resource
    TrainCourseMapper trainCourseMapper;

    @Resource
    TrainCourseServer server;
    @org.junit.Test
    public void test() {
        List<TrainCourse> lists = trainCourseMapper.
                findCourseByCourseClassic("A-培训引导类");
        for (int i =0; i <lists.size();i++){
            System.out.println(lists.get(i).getCourseId());
        }
        System.out.println(lists.get(0).getCreateDate());
    }

    @org.junit.Test
    public void generateId(){

    }
}
