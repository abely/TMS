package com.sanmina.tms.util;




import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by abely on 16-2-22.
 */
public class DBConection {

    public static Connection getDBConnection() throws ClassNotFoundException, SQLException {
        String url="jdbc:postgresql://172.26.100.12:5432/TMSdb";
        String user="postgres";
        String pass="postgres";
        Class.forName("org.postgresql.Driver");
        Connection conn = DriverManager.getConnection(url,user,pass);
        return conn;
    }

    public static void main(String[] args) {
        try {
            DBConection.getDBConnection();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
