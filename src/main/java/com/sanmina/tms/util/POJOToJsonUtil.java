package com.sanmina.tms.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sanmina.tms.domain.TrainCourse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by abely on 16-3-21.
 */
public class POJOToJsonUtil {
    public static String trainCourse(List<TrainCourse> trainCourses){
        Map m = new HashMap();
        m.put("courses",trainCourses);
        String jsonText = JSON.toJSONString(m);
        return jsonText;
    }

}
