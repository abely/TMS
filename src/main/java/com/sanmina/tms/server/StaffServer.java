package com.sanmina.tms.server;

import com.sanmina.tms.dao.PersonMapper;
import com.sanmina.tms.domain.Person;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by abely on 16-5-6.
 */
public class StaffServer {

    @Resource
    private PersonMapper personMapper;

    public List<Person> getAllStaff(){
        return personMapper.findAllPerson();
    }

    public List<Person> getAllStaff(int start, int limit){
        return personMapper.findAllPerson(start, limit);
    }
}
