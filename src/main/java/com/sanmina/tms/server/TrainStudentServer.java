package com.sanmina.tms.server;

import com.sanmina.tms.dao.TrainStudentMapper;
import com.sanmina.tms.domain.TrainStudent;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by abely on 16-5-6.
 */
public class TrainStudentServer {

    @Resource
    private TrainStudentMapper trainStudentMapper;

    public List<TrainStudent> getAllTrainStudent(){
        return trainStudentMapper.findAllTrainStudent();
    }

    public List<TrainStudent> getAllTrainStudent(int start, int limit){
        return trainStudentMapper.findAllTrainStudent(start, limit);
    }
}
