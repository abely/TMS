package com.sanmina.tms.server;

import com.sanmina.tms.dao.TrainCourseMapper;
import com.sanmina.tms.domain.TrainCourse;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by abely on 16-3-17.
 */
@Service
public class CourseInfoImpl {
    @Resource
    TrainCourseMapper trainCourseMapper;

    public List<TrainCourse> getAllPerson(){
        List<TrainCourse> courses = trainCourseMapper.findAllCourse();
        return courses;
    }

    public List<TrainCourse> getAllPerson(int start, int limit){
        List<TrainCourse> courses = trainCourseMapper.findAllCourse(start,limit);
        return courses;

    }

    public TrainCourse findCourseByPrimaryKey(int primaryKey){
        return null;
    }

    public TrainCourse findCourseByCourseId(String courseId){
        return null;
    }




}
