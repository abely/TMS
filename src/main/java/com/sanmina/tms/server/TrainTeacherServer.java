package com.sanmina.tms.server;

import com.sanmina.tms.dao.TrainTeacherMapper;
import com.sanmina.tms.domain.TrainTeacher;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by abely on 16-5-6.
 */
public class TrainTeacherServer {

    @Resource
    private TrainTeacherMapper trainTeacherMapper;

    public List<TrainTeacher> getAllTrainTeacher(){
        return trainTeacherMapper.findAllTrainTeacher();
    }

    public List<TrainTeacher> getAllTrainTeacher(int start, int limit){
        return trainTeacherMapper.findAllTrainTeacher(start, limit);
    }

}
