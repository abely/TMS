package com.sanmina.tms.server;

import com.sanmina.tms.dao.TrainRecrodMapper;
import com.sanmina.tms.domain.TrainRecrod;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by abely on 16-5-6.
 */
public class TrainRecrodServer {

    @Resource
    private TrainRecrodMapper trainRecrodMapper;

    public List<TrainRecrod> getAllTrainRecrod(){
        return trainRecrodMapper.findAllTrainRecrod();
    }

    public List<TrainRecrod> getAllTrainRecrod(int start, int limit){
        return trainRecrodMapper.findAllTrainRecrod(start, limit);
    }
}
